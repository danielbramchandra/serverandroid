package com.example.serverandroid.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.example.serverandroid.Repository.RoutePlanRepository;
import com.example.serverandroid.model.RoutePlan;
import com.example.serverandroid.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class RoutePlanController {
    @Autowired
    RoutePlanRepository repository;

    @PostMapping(value = "/routeplan/save")
    public void saveRoutePlan(@RequestBody RoutePlan routePlan) {
        repository.save(routePlan);
    }

    @PostMapping(value = "/routeplan/getByUser/now")
    public List<RoutePlan> getByUserDateNow(@RequestBody User user) {
        LocalDate date = LocalDate.now();
        List<RoutePlan> listUser = repository.findAllByUserAndDateAndDeletedFalseAndStatusFalse(user, date);
        return listUser;
    }

    @PostMapping(value = "/routeplan/getByUser/{date}")
    public List<RoutePlan> getByUserDateCustom(@RequestBody User user, @PathVariable("date") String inputDate) {

        List<RoutePlan> listUser = repository.findAllByUserAndDateAndDeletedFalse(user, LocalDate.parse(inputDate));
        return listUser;
    }

    @PostMapping(value = "/routeplan/getByUser")
    public List<RoutePlan> getByUser(@RequestBody User user) {
        return repository.findAllByUserAndDeletedFalse(user);
    }

    @GetMapping(value = "/routeplan/delete/{id}")
    public void delete(@PathVariable("id") int id) {
        RoutePlan temp = repository.findById(id).get();
        temp.setDeleted(true);
        repository.save(temp);
    }

    @GetMapping("/routeplan/update/status")
    public boolean updateRoutePlanStatus(@RequestParam int id) {
        try {
            RoutePlan routePlan = repository.findById(id).get();
            routePlan.setStatus(true);
            repository.save(routePlan);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
