package com.example.serverandroid.Controller;

import java.util.List;
import java.util.Optional;

import com.example.serverandroid.Repository.SurveyRepository;
import com.example.serverandroid.model.Customer;
import com.example.serverandroid.model.Survey;

import com.example.serverandroid.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
public class SurveyController {
    @Autowired
    SurveyRepository repo;

    @GetMapping(value = "/survey")
    public List<Survey> getAllSurveys() {
        return (List<Survey>) repo.findAllByDeletedFalse();
    }

    @PostMapping(value = "/survey/user")
    public List<Survey> getAllSurveyByUser(@RequestBody User user) {
        return repo.findAllByUserAndDeletedFalse(user);
    }

    @PostMapping(value = "/survey/save")
    public void saveSurvey(@RequestBody Survey survey) {
        repo.save(survey);
    }

    @GetMapping(value = "/survey/delete/{id}")
    public void delete(@PathVariable("id") int id) {
        Survey temp = repo.findById(id).get();
        temp.setDeleted(true);
        repo.save(temp);
    }

    
}
