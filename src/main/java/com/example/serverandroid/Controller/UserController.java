package com.example.serverandroid.Controller;

import java.io.File;
import java.sql.SQLDataException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import com.example.serverandroid.Repository.CustomerRepository;
import com.example.serverandroid.Repository.UserRepository;
import com.example.serverandroid.model.Customer;
import com.example.serverandroid.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;

@RestController
public class UserController {

    @Autowired
    UserRepository repo;
    @Autowired
    CustomerRepository repoCustomer;

    @GetMapping(value = "/user/delete/{id}")
    public void delete(@PathVariable("id") int id) {
        User temp = repo.findById(id).get();
        temp.setDeleted(true);
        repo.save(temp);
    }

    @GetMapping(value = "/user/{id}")
    public User getCustomer(@PathVariable("id") int id) {
        return repo.findById(id).get();
    }

    @GetMapping(value = "/user")
    public List<User> findAllUser() {
        List<User> data = new LinkedList<>();
        List<User> temp = (List<User>) repo.findAllByDeletedFalse();
        for (User user : temp) {
            if (!user.getRole().matches("Admin")) {
                data.add(user);
            }
        }
        return data;
    }

    @PostMapping(value = "/user/auth")
    public User authLoginAdmin(@RequestBody User userFront) {
        User user = repo.findByUsernameAndDeletedFalse(userFront.getUsername());
        if (user != null && user.getRole().equals("Admin")) {
            if (user.getPassword().equals(userFront.getPassword())) {
                return user;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @PostMapping(value = "/user/auth/sales")
    public User authLoginSales(@RequestBody User userFront) {
        User user = repo.findByUsernameAndDeletedFalse(userFront.getUsername());
        if (user != null) {
            if (user.getPassword().matches(userFront.getPassword())) {
                return user;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @PostMapping(value = "/user/save")
    public String saveUser(@RequestBody User user) {
        User uname = repo.findByUsernameAndDeletedFalse(user.getUsername());
        User email = repo.findByEmailAndDeletedFalse(user.getEmail());
        if (uname != null && email != null) {
            return "username email exist";
        } else if (null != uname) {
            return "username exist";
        } else if (null != email) {
            return "email exist";
        } else {
            repo.save(user);
            return "success";
        }

    }

    @PutMapping(value = "/user/update")
    public void updateUser(@RequestBody User user) {
        repo.save(user);
    }

    @PostMapping(value = "/user/saveWithPhoto")
    public boolean saveUser(@RequestPart("image") MultipartFile file, @RequestPart("user") User user) {
        try {

            String pathFile = this.getClass().getResource("/public/profilepicture/").getPath()
                    + file.getOriginalFilename();
            File saveFile = new File(pathFile.replace("%20", " "));
            file.transferTo(saveFile);
            user.setUrlPhoto("http://localhost:8080/profilepicture/" + file.getOriginalFilename());
            repo.save(user);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    @GetMapping(value = "/user/uname")
    public User findByUname(@RequestParam String uname) {
        return repo.findByUsernameAndDeletedFalse(uname);
    }

    @GetMapping(value = "/user/id")
    public User findById(@RequestParam int id) {
        return repo.findById(id).get();
    }

}