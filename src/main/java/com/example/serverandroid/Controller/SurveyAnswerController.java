package com.example.serverandroid.Controller;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.example.serverandroid.Repository.CustomerRepository;
import com.example.serverandroid.Repository.SurveyAnswerRepository;
import com.example.serverandroid.Repository.SurveyRepository;
import com.example.serverandroid.model.Customer;
import com.example.serverandroid.model.Survey;
import com.example.serverandroid.model.SurveyAnswer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import com.example.serverandroid.Repository.SurveyAnswerRepository;
import com.example.serverandroid.model.SurveyAnswer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SurveyAnswerController {
    @Autowired
    SurveyAnswerRepository repositoryAnswer;
    @Autowired
    CustomerRepository repositoryCustomer;

    @GetMapping(value = "/surveyanswer/getuser")
    public List<SurveyAnswer> getCustomerFromSales(@RequestParam String user, @RequestParam String date) {
        return repositoryAnswer.getCustomer(user, date);
    }

    @GetMapping(value = "/surveyanswer/getdate")
    public List<SurveyAnswer> getDateFromSales(@RequestParam String user) {
        return repositoryAnswer.getAnswerDate(user);
    }

    @GetMapping(value = "/surveyanswer/customer")
    public List<SurveyAnswer> getSurveyAnswerByCustomer(@RequestParam String user, @RequestParam String customer,
            @RequestParam String date) {       
        return repositoryAnswer.getAnswerList(user, date, customer);
    }

    @PostMapping(value = "/surveyans/save")
    public boolean saveSurveyAnswer(@RequestBody List<SurveyAnswer> surveyAnswers) {
        try {
            for (SurveyAnswer surveyAnswer : surveyAnswers) {
                repositoryAnswer.save(surveyAnswer);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
