package com.example.serverandroid.Controller;

import java.io.File;
import java.util.List;
import java.util.Optional;

import com.example.serverandroid.Repository.TaggingRepository;
import com.example.serverandroid.model.Tagging;
import com.example.serverandroid.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@RestController
public class TaggingController {
    @Autowired
    TaggingRepository repo;

    @GetMapping(value = "/tagging")
    public List<Tagging> getAllTagging() {
        return (List<Tagging>) repo.findAll();
    }

    @PostMapping(value = "/tagging/save")
    public boolean saveTagging(@RequestPart("image") List<MultipartFile> file,
            @RequestPart("tagging") List<Tagging> tagging) {
        try {
            if (tagging.size() != file.size()) {
                return false;
            } else {
                for (int i = 0; i < file.size(); i++) {
                    String pathFile = this.getClass().getResource("/public/img/").getPath()
                            + tagging.get(i).getUrlPhoto();
                    File saveFile = new File(pathFile.replace("%20", " "));
                    file.get(i).transferTo(saveFile);
                    tagging.get(i).setUrlPhoto("http://localhost:8080/img/" + tagging.get(i).getUrlPhoto());
                    repo.save(tagging.get(i));
                }
                return true;
            }
        } catch (Exception e) {
            return false;
        }
    }

    @PostMapping(value = "/tagging/user")
    public List<Tagging> getAllTaggingByUser(@RequestBody User user) {
        return repo.findAllByUserAndDeletedFalse(user);
    }

    @PostMapping(value = "/tagging/delete")
    public void deleteTagging(@RequestBody Tagging tagging) {
        repo.delete(tagging);
    }

    @GetMapping(value = "/tagging/findbyid")
    public Tagging getTaggingById(@RequestParam String id) {
        return repo.findById(Integer.parseInt(id)).get();
    }

}
