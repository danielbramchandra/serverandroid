package com.example.serverandroid.Controller;

import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.time.LocalDate;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RestController
public class GeneralController {
    @GetMapping(value = "/datenow")
    public LocalDate getDateNow() {
        return LocalDate.now();
    }

    @GetMapping(value = "/timenow")
    public Long getTimeNow() {
        return System.currentTimeMillis() - 1000;
    }

}
