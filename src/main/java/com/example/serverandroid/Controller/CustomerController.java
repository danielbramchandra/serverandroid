package com.example.serverandroid.Controller;

import java.util.List;
import java.util.Optional;
import com.example.serverandroid.Repository.CustomerRepository;
import com.example.serverandroid.Repository.UserRepository;
import com.example.serverandroid.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CustomerController {
    @Autowired
    CustomerRepository repo;
    @Autowired
    UserRepository repoUser;

    @GetMapping(value = "/customer")
    public List<Customer> getAllCustomers() {
        return (List<Customer>) repo.findAllByDeletedFalse();
    }

    @PostMapping(value = "/customer/save")
    public void saveCustomer(@RequestBody Customer customer) {
        repo.save(customer);
    }

    @GetMapping(value = "/customer/delete/{id}")
    public void delete(@PathVariable("id") int id) {
        Customer temp = repo.findById(id).get();
        temp.setDeleted(true);
        repo.save(temp);
    }


}