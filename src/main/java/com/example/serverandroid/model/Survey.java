package com.example.serverandroid.model;

import java.time.LocalDate;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Survey {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    @Column(name = "surveyQuestion")
    String surveyQuestion;
    @Column(name = "type")
    String type;
    @Column(name = "surveyOption")
    String surveyOption;
    @ManyToOne
    User user;
    boolean deleted = false;

    public Survey() {
    }

    public Survey(int id, String surveyQuestion, String type, String surveyOption, User user, boolean deleted) {
        this.id = id;
        this.surveyQuestion = surveyQuestion;
        this.type = type;
        this.surveyOption = surveyOption;
        this.user = user;
        this.deleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSurveyQuestion() {
        return surveyQuestion;
    }

    public void setSurveyQuestion(String surveyQuestion) {
        this.surveyQuestion = surveyQuestion;

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSurveyOption() {
        return surveyOption;
    }

    public void setSurveyOption(String surveyOption) {
        this.surveyOption = surveyOption;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

}
