package com.example.serverandroid.model;

import java.time.LocalDate;

import javax.persistence.*;

@Entity
public class SurveyAnswer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    String answer;
    @ManyToOne
    Survey survey;
    @ManyToOne
    Customer customer;
    LocalDate answerDate;

    boolean deleted = false;

    public SurveyAnswer() {
    }

    public SurveyAnswer(int id, String answer, Survey survey, Customer customer, LocalDate answerDate,
            boolean deleted) {
        this.id = id;
        this.answer = answer;
        this.survey = survey;
        this.customer = customer;
        this.answerDate = answerDate;
        this.deleted = deleted;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getAnswerDate() {
        return answerDate;
    }

    public void setAnswerDate(LocalDate answerDate) {
        this.answerDate = answerDate;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public Survey getSurvey() {
        return survey;
    }

    public void setSurvey(Survey survey) {
        this.survey = survey;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }
}
