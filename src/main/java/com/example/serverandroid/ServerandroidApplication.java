package com.example.serverandroid;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ServerandroidApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerandroidApplication.class, args);
	}

}
