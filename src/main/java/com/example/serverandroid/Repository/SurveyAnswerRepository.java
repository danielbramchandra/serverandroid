package com.example.serverandroid.Repository;

import java.util.List;

import com.example.serverandroid.model.Customer;
import com.example.serverandroid.model.SurveyAnswer;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.example.serverandroid.model.SurveyAnswer;
import org.springframework.data.repository.CrudRepository;

public interface SurveyAnswerRepository extends CrudRepository<SurveyAnswer, Integer> {
    List<SurveyAnswer> findByCustomer(Customer customer);

    @Query(nativeQuery = true, value = "SELECT * FROM survey_answer LEFT JOIN survey ON survey_answer.survey_id=survey.id WHERE survey.user_id=:user AND answer_date=:date GROUP BY customer_id,answer_date")
    List<SurveyAnswer> getCustomer(@Param("user") String user, @Param("date") String date);

    @Query(nativeQuery = true, value = "SELECT * FROM survey_answer LEFT JOIN survey ON survey_answer.survey_id=survey.id WHERE customer_id=:customer AND survey.user_id=:user AND answer_date=:date")
    List<SurveyAnswer> getAnswerList(@Param("user") String user, @Param("date") String date,
            @Param("customer") String customer);

    @Query(nativeQuery = true, value = "SELECT * FROM survey_answer LEFT JOIN survey ON survey_answer.survey_id=survey.id WHERE survey.user_id = :user GROUP BY answer_date")
    List<SurveyAnswer> getAnswerDate(@Param("user") String user);

    SurveyAnswer save(SurveyAnswer surveyAnswer);
}
