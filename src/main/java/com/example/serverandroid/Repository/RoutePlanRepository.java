package com.example.serverandroid.Repository;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.example.serverandroid.model.RoutePlan;
import com.example.serverandroid.model.User;

import org.springframework.data.repository.CrudRepository;

public interface RoutePlanRepository extends CrudRepository<RoutePlan, Integer> {
    List<RoutePlan> findAllByUserAndDateAndDeletedFalseAndStatusFalse(User user, LocalDate date);

    List<RoutePlan> findAllByUserAndDateAndDeletedFalse(User user, LocalDate date);

    List<RoutePlan> findAllByUserAndDeletedFalse(User user);
}
