package com.example.serverandroid.Repository;

import java.util.List;

import com.example.serverandroid.model.User;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByUsernameAndDeletedFalse(String uname);

    User findByEmailAndDeletedFalse(String email);

    List<User> findAllByDeletedFalse();
}
