package com.example.serverandroid.Repository;

import java.util.List;

import com.example.serverandroid.model.Customer;

import com.example.serverandroid.model.User;
import org.springframework.data.repository.CrudRepository;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    Customer findByIdAndDeletedFalse(int id);

    List<Customer> findAllByDeletedFalse();
}
