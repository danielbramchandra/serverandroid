package com.example.serverandroid.Repository;

import java.util.List;

import com.example.serverandroid.model.Customer;
import com.example.serverandroid.model.Survey;

import com.example.serverandroid.model.User;
import org.springframework.data.repository.CrudRepository;

public interface SurveyRepository extends CrudRepository<Survey,Integer>{    
    List<Survey> findAllByDeletedFalse();
    List<Survey> findAllByUserAndDeletedFalse(User user);
}
