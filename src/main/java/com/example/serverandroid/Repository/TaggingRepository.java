package com.example.serverandroid.Repository;

import java.util.List;

import com.example.serverandroid.model.Tagging;
import com.example.serverandroid.model.User;

import org.springframework.data.repository.CrudRepository;

public interface TaggingRepository extends CrudRepository<Tagging, Integer> {
    List<Tagging> findAllByUserAndDeletedFalse(User user);
}
